package org.jakarta.mode_choice.parameters;

import org.eqasim.core.simulation.mode_choice.parameters.ModeParameters;

public class JakartaModeParameters extends ModeParameters {
	public class JakartaWalkParameters {
		public double alpha_age = 0.0;

	}

	public class JakartaPTParameters {
		public double alpha_age = 0.0;

	}

	public class JakartaIncomeElasticity {
		public double lambda_income = 0.0;
	}

	public class JakartaAvgHHLIncome {
		public double avg_hhl_income = 0.0;
	}

	public class JakartaCarodtParameters {
		public double beta_TravelTime_u_min = 0.0;

		public double betaAccessEgressWalkTime_min = 0.0;
		public double betaWaitingTime_u_min = 0.0;
		public double alpha_u = 0.0;
		public double alpha_sex = 0.0;
		public double alpha_age = 0.0;

	}

	public class JakartaMcodtParameters {
		public double alpha_age = 0.0;
		public double beta_TravelTime_u_min = 0.0;

		public double betaAccessEgressWalkTime_min = 0.0;
		public double betaWaitingTime_u_min = 0.0;
		public double alpha_u = 0.0;
		public double alpha_sex = 0.0;

	}

	public class JakartaMotorcycleParameters {
		public double alpha_age = 0.0;
		public double beta_TravelTime_u_min = 0.0;

		public double betaAccessEgressWalkTime_min = 0.0;
		public double betaWaitingTime_u_min = 0.0;
		public double alpha_u = 0.0;

	}

	public final JakartaWalkParameters jWalk = new JakartaWalkParameters();
	public final JakartaPTParameters jPT = new JakartaPTParameters();
	public final JakartaIncomeElasticity jIncomeElasticity = new JakartaIncomeElasticity();
	public final JakartaAvgHHLIncome jAvgHHLIncome = new JakartaAvgHHLIncome();
	public final JakartaCarodtParameters jCarodt = new JakartaCarodtParameters();
	public final JakartaMcodtParameters jMcodt = new JakartaMcodtParameters();
	public final JakartaMotorcycleParameters jMotorcycle = new JakartaMotorcycleParameters();

	public static JakartaModeParameters buildDefault() {
		JakartaModeParameters parameters = new JakartaModeParameters();

		// Cost
		parameters.betaCost_u_MU = -2.08 / 100;
		parameters.lambdaCostEuclideanDistance = -0.75;
		parameters.referenceEuclideanDistance_km = 7.67;
		parameters.jIncomeElasticity.lambda_income = -0.06;
		parameters.jAvgHHLIncome.avg_hhl_income = 5327;

		// Car
		parameters.car.alpha_u = -0.50;
		parameters.car.betaTravelTime_u_min = -1.24 / 100;

		parameters.car.constantAccessEgressWalkTime_min = 0.0;
		parameters.car.constantParkingSearchPenalty_min = 0.0;

		// PT
		parameters.pt.alpha_u = -3.50;
		parameters.pt.betaLineSwitch_u = 0.0;
		parameters.pt.betaInVehicleTime_u_min = -1.49 / 100;
		parameters.pt.betaWaitingTime_u_min = -1.49 / 100;
		parameters.pt.betaAccessEgressTime_u_min = -1.49 / 100;

		// Bike
		parameters.bike.alpha_u = -4.44;
		parameters.bike.betaTravelTime_u_min = -9.05 / 100;

		// Walk
		parameters.walk.alpha_u = -2.50;
		parameters.walk.betaTravelTime_u_min = -0.52 / 100;
		parameters.jWalk.alpha_age = 1.03 / 100;

		// Carodt
		parameters.jCarodt.beta_TravelTime_u_min = -6.26 / 100;
		parameters.jCarodt.alpha_u = -1.23;
		parameters.jCarodt.alpha_sex = -0.42;
		parameters.jCarodt.alpha_age = -1.32 / 100;

		// Mcodt
		parameters.jMcodt.beta_TravelTime_u_min = -6.26 / 100;
		parameters.jMcodt.alpha_u = -1.15;
		parameters.jMcodt.alpha_sex = -0.42;
		parameters.jMcodt.alpha_age = -1.32 / 100;

		// Motorcycle
		parameters.jMotorcycle.beta_TravelTime_u_min = -3.32 / 100;
		parameters.jMotorcycle.alpha_u = 0.0;
		parameters.jMotorcycle.alpha_age = -0.83 / 100;

		return parameters;
	}
}
