package org.jakarta.mode_choice.parameters;

import org.eqasim.core.simulation.mode_choice.ParameterDefinition;

public class JakartaCostParameters implements ParameterDefinition {
	
	public double carCost_KIDR_km;
	public double motorcycleCost_KIDR_km;
	
	public double ptCostPerTrip_0Transfers_KIDR;
	public double ptCostPerTrip_3Transfers_KIDR;
	
	public double carodtPickUpFee_KIDR;
	public double carodtCostPerMin_KIDR;
	public double carodtCostPerkm_KIDR;
	
	
	public double mcodtPickUpFee_KIDR;
	public double mcodtCostPerMin_KIDR;
	public double mcodtCostPerkm_KIDR;
	

	public double carodtMinCost_KIDR;
	public double mcodtMinCost_KIDR;

	public static JakartaCostParameters buildDefault() {
		JakartaCostParameters parameters = new JakartaCostParameters();

		parameters.carCost_KIDR_km = 2.95;
		parameters.motorcycleCost_KIDR_km = 0.59;
		
		parameters.ptCostPerTrip_0Transfers_KIDR = 4.0;
		parameters.ptCostPerTrip_3Transfers_KIDR = 10.0;
		

		parameters.carodtPickUpFee_KIDR = 6;
		parameters.carodtCostPerkm_KIDR = 4.5;
		parameters.carodtMinCost_KIDR = 10;
		
		parameters.mcodtPickUpFee_KIDR = 4.0;
		parameters.mcodtCostPerkm_KIDR = 2.5;
		parameters.mcodtMinCost_KIDR = 6.0;
		
		
		
		return parameters;
	}
}
