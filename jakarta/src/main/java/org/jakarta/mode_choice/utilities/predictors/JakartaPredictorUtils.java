package org.jakarta.mode_choice.utilities.predictors;

import org.jakarta.mode_choice.parameters.JakartaModeParameters;
import org.matsim.api.core.v01.population.Person;

public class JakartaPredictorUtils {

	static public Double hhlIncome(Person person, JakartaModeParameters parameters) {
		Double hhlIncome = (Double) person.getAttributes().getAttribute("hhlIncome");
		if (hhlIncome != null) {
			return hhlIncome;
		}
		return parameters.jAvgHHLIncome.avg_hhl_income;
	}

	static public int age(Person person, JakartaModeParameters parameters) {
		int age = (int) person.getAttributes().getAttribute("age");
		return age;
	}

	static public String sex(Person person, JakartaModeParameters parameters) {
		String sex = (String) person.getAttributes().getAttribute("sex");
		return sex;
	}

}
