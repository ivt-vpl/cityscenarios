package org.jakarta.mode_choice.utilities.variables;

import org.eqasim.core.simulation.mode_choice.utilities.variables.BaseVariables;

public class JakartaPersonVariables implements BaseVariables {
	public double hhlIncome;
	public int age;
	public String sex;

	public JakartaPersonVariables(double hhlIncome, int age, String sex) {
		this.hhlIncome = hhlIncome;
		this.age = age;
		this.sex = sex;
	}
}
