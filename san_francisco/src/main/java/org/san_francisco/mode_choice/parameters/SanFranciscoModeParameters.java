package org.san_francisco.mode_choice.parameters;

import org.eqasim.core.simulation.mode_choice.parameters.ModeParameters;

public class SanFranciscoModeParameters extends ModeParameters {
	
	public class SanFranciscoWalkParameters {
		public double alpha_walk_city = 0.0;
		public double vot_min = 0.0;
	}
	
	public class SanFranciscoCarParameters {
		public double vot_min = 0.0;
	}
	
	public class SanFranciscoPTParameters {
		public double alpha_pt_city = 0.0;
		public double vot_min = 0.0;
	}
	
	public class SanFranciscoIncomeElasticity {
		public double lambda_income = 0.0;
	}
	
	public class SanFranciscoAvgHHLIncome {
		public double avg_hhl_income = 0.0;
	}
	
	public final SanFranciscoWalkParameters sfWalk = new SanFranciscoWalkParameters();
	public final SanFranciscoPTParameters sfPT = new SanFranciscoPTParameters();
	public final SanFranciscoCarParameters sfCar = new SanFranciscoCarParameters();
	public final SanFranciscoIncomeElasticity sfIncomeElasticity = new SanFranciscoIncomeElasticity();
	public final SanFranciscoAvgHHLIncome sfAvgHHLIncome = new SanFranciscoAvgHHLIncome();

	public static SanFranciscoModeParameters buildDefault() {
		SanFranciscoModeParameters parameters = new SanFranciscoModeParameters();
		
		return parameters;
	}
}
