package org.los_angeles.mode_choice.parameters;

import org.eqasim.core.simulation.mode_choice.parameters.ModeParameters;

public class LosAngelesModeParameters extends ModeParameters {
	
	public class LosAngelesWalkParameters {
		public double alpha_walk_city = 0.0;
		public double vot_min = 0.0;
	}
	
	public class LosAngelesPTParameters {
		public double alpha_pt_city = 0.0;
		public double vot_min = 0.0;
		public double alpha_orange_county = 0.0;
	}
	
	public class LosAngelesCarParameters {
		public double vot_min = 0.0;
	}
	
	public class LosAngelesIncomeElasticity {
		public double lambda_income = 0.0;
	}
	
	public class LosAngelesAvgHHLIncome {
		public double avg_hhl_income = 0.0;
	}
	
	public final LosAngelesWalkParameters laWalk = new LosAngelesWalkParameters();
	public final LosAngelesPTParameters laPT = new LosAngelesPTParameters();
	public final LosAngelesCarParameters laCar = new LosAngelesCarParameters();
	public final LosAngelesIncomeElasticity laIncomeElasticity = new LosAngelesIncomeElasticity();
	public final LosAngelesAvgHHLIncome laAvgHHLIncome = new LosAngelesAvgHHLIncome();

	public static LosAngelesModeParameters buildDefault() {
		LosAngelesModeParameters parameters = new LosAngelesModeParameters();
				
		return parameters;
	}
}
